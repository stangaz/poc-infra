notes

- set binlog retention to 24 hours 
```sql
call mysql.rds_set_configuration('binlog retention hours', 24);
```