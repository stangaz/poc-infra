variable "vpc_name" {
  description = "Name of vpc"
  type        = string
  default     = "orderup-db-test"
}

variable "region" {
  description = "AWS Region"
  type        = string
  default     = "ap-southeast-2"
}

variable "bastion_ingress_cidr" {
  description = "ingress cidr for bastion sg"
  type        = string
  default     = "116.255.32.72/32"
}
