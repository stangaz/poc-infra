resource "aws_security_group" "orderup-db" {
  name        = "orderup-db-sg"
  description = "Allow TLS inbound traffic"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description = "TLS from VPC"
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = concat(module.vpc.public_subnets_cidr_blocks, module.vpc.private_subnets_cidr_blocks)
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_db_parameter_group" "orderup-db" {
  name   = "rds-pg"
  family = "mysql5.7"

  parameter {
    name  = "log_bin_trust_function_creators"
    value = "1"
  }

  parameter {
    name  = "binlog_format"
    value = "ROW"
  }

  parameter {
    name  = "binlog_checksum"
    value = "NONE"
  }

  parameter {
    name  = "binlog_row_image"
    value = "full"
  }
}


locals {
  name = "orderupdb"

  engine               = "mysql"
  engine_version       = "5.7.37"
  family               = "mysql5.7"
  major_engine_version = "5.7"

  instance_class        = "db.t2.micro"
  allocated_storage     = 20
  max_allocated_storage = 100
  port                  = "3306"

}

module "db" {
  source = "terraform-aws-modules/rds/aws"

  identifier = "${local.name}-master"

  engine                 = local.engine
  engine_version         = local.engine_version
  instance_class         = local.instance_class
  allocated_storage      = local.allocated_storage
  max_allocated_storage  = local.max_allocated_storage
  db_name                = local.name
  create_random_password = false
  username               = "admin"
  password               = "password"
  port                   = local.port

  apply_immediately = true

  backup_window           = "23:30-01:30"
  backup_retention_period = 1
  maintenance_window      = "Mon:02:00-Mon:03:00"

  iam_database_authentication_enabled = false
  storage_encrypted                   = false
  vpc_security_group_ids              = [aws_security_group.orderup-db.id]

  create_monitoring_role = false

  tags = {
    Owner       = "user"
    Environment = "dev"
  }

  # DB subnet group
  create_db_subnet_group = true
  subnet_ids             = module.vpc.private_subnets

  # DB parameter group
  family = local.family

  # DB option group
  major_engine_version = local.major_engine_version

  # Database Deletion Protection
  deletion_protection = false

  create_db_parameter_group = false
  parameter_group_name      = aws_db_parameter_group.orderup-db.id
}

module "replica" {
  source = "terraform-aws-modules/rds/aws"

  identifier = "${local.name}-replica"

  replicate_source_db   = module.db.db_instance_id
  engine                = local.engine
  engine_version        = local.engine_version
  instance_class        = local.instance_class
  allocated_storage     = local.allocated_storage
  max_allocated_storage = local.max_allocated_storage

  create_random_password = false

  apply_immediately = true

  port                                = local.port
  iam_database_authentication_enabled = false
  storage_encrypted                   = false
  vpc_security_group_ids              = [aws_security_group.orderup-db.id]

  maintenance_window      = "Mon:02:00-Mon:03:00"
  backup_window           = "23:30-01:30"
  backup_retention_period = 1

  create_monitoring_role = false

  tags = {
    Owner       = "user"
    Environment = "dev"
  }

  # DB parameter group
  family = local.family

  # DB option group
  major_engine_version = local.major_engine_version

  # Database Deletion Protection
  deletion_protection = false

  create_db_parameter_group = false
  parameter_group_name      = aws_db_parameter_group.orderup-db.id
}
