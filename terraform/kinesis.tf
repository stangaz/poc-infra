resource "aws_kinesis_stream" "dms" {
  name = "dms-cdc"

  shard_level_metrics = [
    "IncomingBytes",
    "OutgoingBytes",
  ]

  stream_mode_details {
    stream_mode = "ON_DEMAND"
  }
}
