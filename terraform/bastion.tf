resource "aws_security_group" "bastion" {
  description = "Allow ssh inbound traffic"
  vpc_id      = module.vpc.vpc_id
  name_prefix = "bastion-"
  lifecycle {
    create_before_destroy = true
  }

  ingress {
    description = "ssh from home"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.bastion_ingress_cidr]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_iam_instance_profile" "bastion" {
  name = "bastion"
  role = aws_iam_role.bastion.name
}

data "aws_iam_policy_document" "bastion" {
  statement {
    actions = [
      "s3:PutObject",
      "s3:GetObject",
      "s3:ListBucket",
      "s3:DeleteObject"
    ]

    resources = [
      "${aws_s3_bucket.storage.arn}/*",
    ]
  }
}

resource "aws_iam_policy" "bastion" {
  name   = "bastion_policy"
  path   = "/"
  policy = data.aws_iam_policy_document.bastion.json
}

data "aws_iam_policy_document" "bastion_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "bastion" {
  name               = "bastion_role"
  path               = "/"
  assume_role_policy = data.aws_iam_policy_document.bastion_assume_role_policy.json
}

resource "aws_iam_role_policy_attachment" "bastion" {
  role       = aws_iam_role.bastion.name
  policy_arn = aws_iam_policy.bastion.arn
}

resource "aws_instance" "bastion" {
  ami                  = "ami-0a4e637babb7b0a86"
  instance_type        = "t2.micro"
  iam_instance_profile = aws_iam_instance_profile.bastion.id
  key_name             = "bastion"

  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.bastion.id]
  subnet_id                   = module.vpc.public_subnets[0]
}
