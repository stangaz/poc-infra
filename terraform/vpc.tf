
locals {
  private_subnets     = ["10.0.0.128/26", "10.0.0.192/26"]
  private_subnet_cidr = "10.0.0.128/25"
  public_subnets      = ["10.0.0.0/25"]
}

################################################################################
# VPC Module
################################################################################

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = var.vpc_name
  cidr = "10.0.0.0/24"

  azs             = ["${var.region}a", "${var.region}b"]
  private_subnets = local.private_subnets
  public_subnets  = local.public_subnets

  enable_ipv6 = false

  enable_nat_gateway = true

  public_subnet_tags = {
    Name = var.vpc_name
  }

  enable_dns_hostnames = true

  vpc_tags = {
    Name = var.vpc_name
  }

  manage_default_network_acl = true


  private_dedicated_network_acl = true
  private_inbound_acl_rules = [
    {
      rule_number = 110
      rule_action = "allow"
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      cidr_block  = element(local.public_subnets, 0)
    },
    {
      rule_number = 120
      rule_action = "allow"
      from_port   = 3306
      to_port     = 3306
      protocol    = "tcp"
      cidr_block  = element(local.public_subnets, 0)
    },
    {
      rule_number = 130
      rule_action = "allow"
      protocol    = "-1"
      cidr_block  = local.private_subnet_cidr
    }
  ]


  public_dedicated_network_acl = true
  public_inbound_acl_rules = [
    {
      rule_number = 110
      rule_action = "allow"
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      cidr_block  = var.bastion_ingress_cidr
    },
    {
      rule_number = 120
      rule_action = "allow"
      from_port   = 3306
      to_port     = 3306
      protocol    = "tcp"
      cidr_block  = local.private_subnet_cidr
    },
    {
      rule_number = 130
      rule_action = "allow"
      from_port   = 443
      to_port     = 443
      protocol    = "tcp"
      cidr_block  = "0.0.0.0/0"
    },
    {
      rule_number = 140
      rule_action = "allow"
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_block  = "0.0.0.0/0"
    },
    {
      rule_number = 150
      rule_action = "allow"
      protocol    = "tcp"
      from_port   = 1024
      to_port     = 65535
      cidr_block  = "0.0.0.0/0"
    },
  ]

}
