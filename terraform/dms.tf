locals {
  cdc_bucket = "arn:aws:s3:::orderup-db"
}

data "aws_iam_policy_document" "dms-kinesis" {
  statement {
    actions = [
      "kinesis:DescribeStream",
      "kinesis:PutRecord",
      "kinesis:PutRecords"
    ]

    resources = [
      aws_kinesis_stream.dms.arn,
    ]
  }
}

resource "aws_iam_policy" "dms-kinesis" {
  name   = "dms_kinesis_policy"
  path   = "/"
  policy = data.aws_iam_policy_document.dms-kinesis.json
}

data "aws_iam_policy_document" "dms-s3" {
  statement {
    actions = [
      "s3:*"
    ]

    resources = [
      "${local.cdc_bucket}/*",
      "${local.cdc_bucket}",
    ]
  }
}

resource "aws_iam_policy" "dms-s3" {
  name   = "dms_s3_policy"
  path   = "/"
  policy = data.aws_iam_policy_document.dms-s3.json
}

data "aws_iam_policy_document" "dms_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["dms.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "dms" {
  name               = "dms_role"
  path               = "/"
  assume_role_policy = data.aws_iam_policy_document.dms_assume_role_policy.json
}

resource "aws_iam_role_policy_attachment" "dms-kinesis" {
  role       = aws_iam_role.dms.name
  policy_arn = aws_iam_policy.dms-kinesis.arn
}

resource "aws_iam_role_policy_attachment" "dms-s3" {
  role       = aws_iam_role.dms.name
  policy_arn = aws_iam_policy.dms-s3.arn
}


resource "aws_iam_role" "dms-cloudwatch-logs-role" {
  assume_role_policy = data.aws_iam_policy_document.dms_assume_role_policy.json
  name               = "dms-cloudwatch-logs-role"
}

resource "aws_iam_role_policy_attachment" "dms-cloudwatch-logs-role-AmazonDMSCloudWatchLogsRole" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonDMSCloudWatchLogsRole"
  role       = aws_iam_role.dms-cloudwatch-logs-role.name
}


resource "aws_security_group" "dms-cdc" {
  description = "Allow traffic in vpc"
  vpc_id      = module.vpc.vpc_id
  name_prefix = "dms-"
  lifecycle {
    create_before_destroy = true
  }

  ingress {
    description = "ssh from bastion"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/25"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_dms_replication_subnet_group" "dms" {
  replication_subnet_group_id          = "dms"
  replication_subnet_group_description = "subnet group for dms"
  subnet_ids                           = module.vpc.private_subnets

}

resource "aws_dms_replication_instance" "dms-cdc" {
  allocated_storage            = 20
  apply_immediately            = true
  auto_minor_version_upgrade   = true
  availability_zone            = "ap-southeast-2a"
  engine_version               = "3.4.6"
  multi_az                     = false
  preferred_maintenance_window = "sun:10:30-sun:14:30"
  publicly_accessible          = true
  replication_instance_class   = "dms.t2.micro"
  replication_instance_id      = "dms-cdc"
  replication_subnet_group_id  = aws_dms_replication_subnet_group.dms.id

  vpc_security_group_ids = [
    aws_security_group.dms-cdc.id,
  ]

  depends_on = [
    aws_iam_role_policy_attachment.dms-s3,
    aws_iam_role_policy_attachment.dms-kinesis,
    aws_iam_role_policy_attachment.dms-cloudwatch-logs-role-AmazonDMSCloudWatchLogsRole,
  ]
}

resource "aws_dms_endpoint" "dms-cdc" {
  endpoint_id                 = "dms-cdc-source-db"
  endpoint_type               = "source"
  engine_name                 = "mysql"
  extra_connection_attributes = ""
  server_name                 = module.replica.db_instance_address
  username                    = "admin"
  password                    = "password"
  port                        = 3306

  ssl_mode = "none"

  tags = {
    Name = "test"
  }

}

resource "aws_dms_endpoint" "dms-s3" {
  endpoint_id   = "dms-cdc-target-s3"
  endpoint_type = "target"

  engine_name                 = "s3"
  extra_connection_attributes = ""

  s3_settings {
    bucket_folder                    = "dms-s3-direct"
    bucket_name                      = "orderup-db"
    cdc_path                         = "cdc"
    data_format                      = "parquet"
    include_op_for_full_load         = true
    parquet_timestamp_in_millisecond = true
    service_access_role_arn          = aws_iam_role.dms.arn

  }
}

resource "aws_dms_replication_task" "dms-cdc" {
  migration_type           = "full-load-and-cdc"
  replication_instance_arn = aws_dms_replication_instance.dms-cdc.replication_instance_arn
  replication_task_id      = "dms-cdc-task"
  source_endpoint_arn      = aws_dms_endpoint.dms-cdc.endpoint_arn
  table_mappings           = "{\"rules\":[{\"rule-type\": \"selection\",\"rule-id\": \"116585469\",\"rule-name\": \"116585469\",\"object-locator\":{\"schema-name\": \"jauoqbix4sbyg4g0ow0b7864m\",\"table-name\":\"%\"},\"rule-action\":\"include\",\"filters\":[]}]}"


  target_endpoint_arn = aws_dms_endpoint.dms-s3.endpoint_arn

  lifecycle {
    ignore_changes = [replication_task_settings]
  }
}

resource "aws_dms_endpoint" "dms-kinesis" {
  endpoint_id   = "dms-cdc-target-kinesis"
  endpoint_type = "target"

  engine_name                 = "kinesis"
  extra_connection_attributes = ""

  kinesis_settings {
    stream_arn              = aws_kinesis_stream.dms.arn
    service_access_role_arn = aws_iam_role.dms.arn
  }
}
